#Para executar o código você deve:
#Instalar o MySQL
#Criar uma tabela
#CREATE DATABASE db_user; 
#USE db_user;
#CREATE TABLE tb_users (
#   nome VARCHAR(20),
#   idade TINYINT,
#   endereco VARCHAR(50)
#);

from flask import Flask, jsonify, request, json
import mysql.connector
from mysql.connector import Error

#Para executar o connector você deve mudar os parâmentros, host, user e password
# para aqueles que estiverem configurados em sua máquina.
bancoDeDados = mysql.connector.connect(host="localhost",user="root",password="Murilo123",database="db_user")

app = Flask(__name__)

@app.route('/listarTodos', methods=['GET'])
def listarAlunos():
    selectAllSql = f"select * from tb_users"
    cursor = bancoDeDados.cursor()
    cursor.execute(selectAllSql)
    resultado = cursor.fetchall()

    return jsonify(resultado)

@app.route('/cadastrar', methods=['POST'])
def cadastrar():
    dados = json.loads(request.data)
    nome = dados.get("nome", None)
    idade = dados.get("idade", None)
    endereco = dados.get("endereco", None)
    query = f"INSERT INTO tb_users (nome, idade, endereco)VALUES (%s, %s, %s)"
    val = (nome, idade, endereco)
    cursor = bancoDeDados.cursor()
    cursor.execute(query, val)
    return "Cadastrado com sucesso!"

if __name__ == '__main__':
    app.run( port=5000, debug=True)