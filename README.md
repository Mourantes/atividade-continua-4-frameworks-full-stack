# Atividade Contínua 4 - Frameworks Full Stack

## Motivo do Repositório
Este é um repositório para armazenar a Atividade Contínua 4 da matéria de Frameworks Fullstack do curso de Sistemas da informação da Faculdade IMPACTA de Tecnologia.

## Objetivo:
Criar uma estrutura no backend em Python onde exista pelo menos 2 rotas (Methods GET e POST) que possam se comunicar com uma estrutura de banco de dados e executando as ações de cada um dos Methods.

## Informações dos Alunos
Nome: Murilo Rodrigues Moura - RA: 2102198 /
Nome: Gabriel Barbosa de Oliveira - RA:2102639